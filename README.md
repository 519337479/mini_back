<p align="center"><a href="" target="_blank"><img  src="https://img.mini.chongyeapp.com/icons/minisns.svg" width="200"></a></p>

## 关于MiniSNS
#### MiniSNS 专注于社区带货   
**关于使用**：改个域名就能变成自己的   
**关于作者**：本人工作岗位是NLP工程师，俗称炼丹侠的ai工程师一种，所以我擅长的是逻辑，算法，后端，并不擅长前端的UI美化，这里真诚的邀请前端大佬和我一起打磨这个产品。我一直想做一个可以永远运营下去的App，无关赚钱，只是读书时候的理想。   
目前市面上真正完善运营级的社交程序不多，很难满足自己的要求，那我们就创造一个吧。   
### 一、演示
（由于大多数时间处于审核中的状态，若开启了审核模式，不会显示视频）   
1、APP端：[https://www.chongyeapp.com](https://www.chongyeapp.com "https://www.chongyeapp.com")  
2、小程序端：微信小程序搜索：宠也网  
![](https://img.mini.chongyeapp.com/images/hbx/minipro.jpg)  
3、后台：[https://cn.chongyeapp.com/admin](https://cn.chongyeapp.com/admin "https://cn.chongyeapp.com/admin")  
账号：admin  
密码：minisns666  
### 二、截图

| 界面 | 界面 |
| --- | --- |
| ![](https://img.mini.chongyeapp.com/images/hbx/1.png)首页 | ![](https://img.mini.chongyeapp.com/images/hbx/17.png)发现页 |
| ![](https://img.mini.chongyeapp.com/images/hbx/3.png)发表图文 | ![](https://img.mini.chongyeapp.com/images/hbx/4.png)发表付费贴 |
| ![](https://img.mini.chongyeapp.com/images/hbx/18.png)登录 | ![](https://img.mini.chongyeapp.com/images/hbx/19.png)搜索页 |
| ![](https://img.mini.chongyeapp.com/images/hbx/5.png)我的 | ![](https://img.mini.chongyeapp.com/images/hbx/23.png)管理页 |
| ![](https://img.mini.chongyeapp.com/images/hbx/7.png)钱包 | ![](https://img.mini.chongyeapp.com/images/hbx/8.png)余额充值 |
| ![](https://img.mini.chongyeapp.com/images/hbx/9.png)详情页1 | ![](https://img.mini.chongyeapp.com/images/hbx/10.png)详情页2 |
| ![](https://img.mini.chongyeapp.com/images/hbx/11.png)话题页 | ![](https://img.mini.chongyeapp.com/images/hbx/12.png)搜索页 |
| ![](https://img.mini.chongyeapp.com/images/hbx/13.png)常见问题 | ![](https://img.mini.chongyeapp.com/images/hbx/14.png)图集详情 |
| ![](https://img.mini.chongyeapp.com/images/hbx/15.png)消费记录 | ![](https://img.mini.chongyeapp.com/images/hbx/20.png)订单（包含虚拟物品） |   

### 三、说明
mini是一个很严格的企业级运营的程序，拥有一个营业执照和一个服务器（不是虚拟机）是最基本的需求，mini强大的地方不是颜值，而是建立在一个非常健康的系统上，稳定运行，为之后的算法实现减少难度，当然小白只能看得懂好不好看，总之，适合小中大型社区平台   
1、**整体的账号体系建立在微信下**，因为微信是最安全，稳定的社交App，利用微信的账号和自己的站内账号一对一绑定，会给你以后的运营带来很多的好处，可以减少灌水贴，利用多账号进行作弊，整体登录也很简单，不会出现用户QQ登录一次，产生一个账号，微信登录一次产生一个账号等烦恼吧，而且你至少可以省掉人机检测，短信验证码的费用，何乐而不为，当然如果后面需要实名认证，可以考虑手机号或者别的方式。   
2、**为什么只能配置腾讯云cos**，因为系统越来越复杂，涉及到图片处理，视频截屏，图片清理等等功能，所以只适配腾讯云cos，不会安排其他云对象存储或者本地。   
3、如果以上你不赞同或者没有营业执照，那我不建议您使用这个系统。   

### 四、开始
1、整个项目分为前端和后端，后端地址：[https://gitee.com/wen-open/mini_back](https://gitee.com/wen-open/mini_back "https://gitee.com/wen-open/mini_back")  
2、前端文档：[https://doc.chongyeapp.com/project-2/](https://doc.chongyeapp.com/project-2/ "https://doc.chongyeapp.com/project-2/")  
后端文档：[https://doc.chongyeapp.com/project-1/](https://doc.chongyeapp.com/project-1/ "https://doc.chongyeapp.com/project-1/")  
3、本项目前端是基于“轻航”的微信小程序源码重写为uniapp代码二开而来，轻航官网：[https://qinghang.supengjun.com/index.html](https://qinghang.supengjun.com/index.html "https://qinghang.supengjun.com/index.html")   
4、进项目交流群请加我微信：ShaoWenSir  
![](https://img.mini.chongyeapp.com/images/mmqrcode1677495496626%281%29.png)  

### 六、开源致谢（排名不分先后）
laravel   
uniapp   
elastic search   
dcat_admin   
linui   
gateway work (workerman)   
mp_html   
轻航小程序   
scss   
laravel-admin   
...